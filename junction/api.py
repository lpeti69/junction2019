import openfoodfacts
import json


def find_product(barcode):
    product = openfoodfacts.get_product(barcode)
    return product['product']


def find_nutriments(product):
    result = dict()
    if 'nutriments' in product:
        result = product['nutriments']

    return result


def find_nutri_score(product):
    result = ""
    if 'nutrition_grades_tags' in product:
        result = product['nutrition_grades_tags']

    return result


def get_all_by_brand(brand):
    products = openfoodfacts.products.get_by_brand(brand)
    write_to_json(products[0])

    return products


def write_to_json(data):
    with open('data.json', 'w') as f:
        json.dump(data, f)


def main():
    barcode = "4011800527228"
    # print find_nutri_score(barcode)['nutriments']
    products = get_all_by_brand("danone")
    product = products[0]
    nutri_score =  find_nutri_score(product)
    print nutri_score
    write_to_json(product)


if __name__ == "__main__":
    main()



